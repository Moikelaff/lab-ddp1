harga_pulpen = 10000
harga_pensil = 5000
harga_cortape = 15000

print(f"""
Toko ini menjual:
1. Pulpen ({harga_pulpen}/pcs)
2. Pensil ({harga_pensil}/pcs)
3. Correction Tape ({harga_cortape}/pcs)
Masukkan jumlah yang ingin dibeli""")

in_pulpen = int(input("pulpen: "))
in_pensil = int(input("pensil: "))
in_cortape = int(input("correction tape: "))
total_harga = ((in_pulpen * harga_pulpen) + (in_pensil * harga_pensil) + (in_cortape * harga_cortape))

print(f"""
Ringkasan Pembelian:
{in_pulpen} pulpen x 10000
{in_pensil} pensil x 5000
{in_cortape} correction tape x 15000
Total harga: {total_harga}
""")