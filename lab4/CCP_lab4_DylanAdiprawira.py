import re

in_name = input("Masukkan nama file input: ")
out_name = input("Masukkan nama file output: ")

#initialize awal
line_total = 0
kept_line = 0
total_num = 0
temp_lst = []
 
try:
    #input
    with open(in_name, "r") as in_file:
        clean_lines = in_file.read()
        temp = clean_lines
        clean_text = re.sub('[^a-zA-Z.\s]', '', clean_lines)
        
        for line in clean_text:
            kept_line += 1

        # untuk cari line total
        lines = temp.split("\n")
        for line in lines:
            line_total += 1

        # iterate per line
        for i in range(len(lines)):
            words = lines[i].split()

            # iterate per word
            for j in range(len(words)):
                word = words[j].split()

                #iterate per letter
                for k in range(len(word)):
                    letter = word[k]
                    # buat menghitung ganjil apa genapnya line
                    if letter.isnumeric():
                        total_num +=1
                        temp_lst.append("")

                    elif letter.isalpha():
                        temp_lst.append(letter)

                    elif letter == " ":
                        temp_lst.append(" ")
                    else:
                        temp_lst.append("")

                word[k] = "".join(temp_lst)
  
            lines[i] = "".join(word)

            # mencari ganjil genap
            if total_num % 2 == 0:
                pass
            else:
                pass
    #output
    with open(out_name, "w") as out_file:
        
        out_file.write(clean_text + "\n")  #TODO: PLACEHOLDER
        out_file.write(f"\nTotal baris dalam file input: {line_total}")
        out_file.write(f"\nTotal baris yang disimpan: {kept_line}\n")


# handling file tidak ada
except FileNotFoundError:
    print("Nama file yang anda masukkan tidak ditemukan :(")
