import random

# initalize variable awal
paragraf = "the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. "
pesan_asli = input("masukkan pesan yang akan dikirim: ")
n = int(input("pilih nilai n: "))
kunci = 20220310-n


# bagian sandi caesar
temp = []
while len(pesan_asli) <= 32:
    for i in range(len(pesan_asli)):
        letter = pesan_asli[i]
        if (letter.isupper()):
            letter = (ord(letter) + n - 65) % 26 + 65
            temp += chr(letter)

        elif (letter.islower()):
            letter = (ord(letter) + n - 97) % 26 + 97
            temp += chr(letter)
            
        elif (letter.isdigit()):
            if letter % 2 == 0:
                letter = (ord(letter) + n - 48) % 26 + 48
                temp += chr(letter)
            else:
                letter = (ord(letter) + n -49) % 26 + 48
                temp += chr(letter)
    if len(pesan_asli) >=32:
        break

kunci = str(kunci)
kunci_position = random.randint(1, 32)
temp = temp.insert(kunci_position, kunci)              
pesan_encrypted = "".join(temp)

# bagian pesan
temp2 =[]
for i in pesan_asli:
    if i in pesan_encrypted:
        if  i.isalpha():
            temp2.append(i.upper())
        elif i == " ":
            temp2.append("$")
        elif  i.isdigit():
            temp2.append(i)
result = "".join(temp2)

encryption_position = random.randint(len(paragraf))

paragraf_encryption = paragraf.split()
paragraf_encryption = paragraf_encryption.insert(encryption_position, pesan_encrypted)
paragraf = "".join(paragraf)
print(paragraf)
