# initialize starting value
ei_hp = 100
raiden_hp = 100
turn = 0
damage_to_ei = 0
damage_to_raiden = 0

turn_info = f"""
HP Ei: {ei_hp}
HP Raiden: {raiden_hp}
Putaran ke {turn}
----------------------------"""

menu ="""
Apa yang ingin anda lakukan?
1. Menyerang
2. Menghindar
3. Musou no Hitotachi
"""
menu_no_musou ="""
Apa yang ingin anda lakukan?
1. Menyerang
2. Menghindar
"""
turn_summary = f"""
Ei menyerang Raiden dan mengurangi HP Raiden sebesar {damage_to_raiden}
Raiden menyerang Ei dan mengurangi HP Ei sebesar {damage_to_ei}
"""
turn_summary_nodmg = "Raiden menyerang namun Ei berhasil menghindar"

turn_summary_musou = f'''
Ei Menyerang Raiden dengan "Musou no Hitotachi dan mengurangi HP Raiden sebesar 50
raiden menyerang Ei dan mengurangi HP ei sebesar {damage_to_ei}
'''

choice = input("Masukkan pilihan anda: ")
choice = int(choice)

# reference urutan interface ||| turn_info -> menu -> choice -> turn_summary |||

while True:
    turn += 1
    print(turn_info)
    print(menu)
    print(choice)

     # turn 1 hp 100 100
    if choice == 1:
        damage_to_raiden = 20
        damage_to_ei = 20
        raiden_hp -= damage_to_raiden
        ei_hp -= damage_to_ei

        turn += 1
        print(turn_summary)
        print(turn_info)
        print(menu)
        print(choice)
        
         # turn 2 hp 80 80 
        if choice == 1:
            damage_to_raiden = 20
            damage_to_ei = 20
            raiden_hp -= damage_to_raiden
            ei_hp -= damage_to_ei
            
            turn += 1
            print(turn_info)
            print('Raiden siap menggunakan "The Final Calamity"')
            print(menu)
            print(choice)

            # turn 3 hp 60 60 
            if choice == 1:
                damage_to_raiden = 20
                damage_to_ei = 50
                raiden_hp -= damage_to_raiden
                ei_hp -= damage_to_ei

                turn += 1
                print(turn_info)
                print(menu)
                print(choice)

                # turn 4 hp 10 40
                if choice == 1:
                    raiden_hp = 20
                    ei_hp = 0
                    print("Raiden memenangkan pertarungan")
                    break

                elif choice == 2: 
                    raiden_hp = raiden_hp
                    ei_hp = ei_hp

                    turn += 1
                    print(turn_info)
                    print(menu)
                    print(choice)
                    break

                elif choice == 3:
                    raiden_hp = 0
                    ei_hp = 0
                    print("pertarungan seri")
                    break
            

            elif choice == 2: 
                turn += 1
                continue

            
            elif choice == 3:
                damage_to_ei = 50
                damage_to_raiden = 50
                ei_hp -= damage_to_ei
                raiden_hp -= damage_to_raiden
                
                turn += 1
                print(turn_info)
                print(menu_no_musou)
                print(choice)

                #turn 4 hp 10 10
                if choice == 1:
                    print("pertarungan seri")
                    break
                elif choice == 2:
                    break

        elif choice == 2:
            raiden_hp = raiden_hp
            ei_hp = ei_hp

            turn += 1
            print(turn_info)
            print('Raiden siap menggunakan "The Final Calamity"')
            print(menu)
            print(choice)
            continue

        elif choice == 3:
            raiden_hp -= 50
            ei_hp -= 20

            turn += 1
            print(turn_info)
            print('Raiden siap menggunakan "The Final Calamity"')
            print(menu_no_musou)
            print(choice)

    elif choice == 2:
        turn += 1
        continue
    
    elif choice == 3:
        damage_to_raiden = 50
        damage_to_ei = 20
        raiden_hp -= damage_to_raiden
        ei_hp -= damage_to_ei

        turn += 1
        print(turn_summary)
        print(turn_info)
        print(menu_no_musou)
        print(choice)
        
        # turn 2 hp 80 50
        if choice == 1:
            damage_to_raiden = 20
            damage_to_ei = 20
            raiden_hp -= damage_to_raiden
            ei_hp -= damage_to_ei

            turn += 1
            print(turn_summary)
            print('raiden siap menggunakan "The Final Calamity')
            print(turn_info)
            print(menu_no_musou)
            print(choice)

            # turn 3 hp 30 10
            if choice == 1:
                damage_to_raiden = 20
                damage_to_ei = 50
                raiden_hp -= damage_to_raiden
                ei_hp -= damage_to_ei

                turn += 1
                print(turn_summary)
                
                print(turn_info)
                print(menu_no_musou)
                print(choice)

                #turn 4 hp 10 0
                if choice == 1:
                    print("Ei memenangkan pertarungan")
                elif choice ==2:
                    turn += 1
                    continue

            elif choice == 2:
                turn += 1
                continue

        if choice == 2:
            turn += 1
            continue
        
