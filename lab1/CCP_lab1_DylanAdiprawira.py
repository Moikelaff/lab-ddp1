# input dan konversi awal
in_cipher = input("Masukkan karakter: ")
angka = ord(in_cipher)
encrypted = angka % 7 * (angka ** 2) % 69 + 666
print(f"Fake Eligma untuk {in_cipher}: {encrypted}" )

# konversi basis encrypted
bin_x = bin(encrypted)
hex_x = hex(encrypted)
bin_x = bin_x[2:]
hex_x = hex_x[2:]
hasil = bin_x + hex_x
print(f"Enkripsi Fake Eligma untuk {in_cipher}: {hasil}")