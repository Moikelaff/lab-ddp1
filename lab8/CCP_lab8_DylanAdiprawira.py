class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.__tipe = tipe
        self.__harga = harga
        self.__tulisan = tulisan
        self.__angka_lilin = angka_lilin
        self.__topping = topping
    
    def get_tipe(self):
        return self.__tipe

    def get_harga(self):
        return self.__harga
    
    def get_tulisan(self):
        return self.__tulisan
    
    def get_angka_lilin(self):
        return self.__angka_lilin
    
    def get_topping(self):
        return self.__topping

class KueSponge(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
       super().__init__(tulisan, angka_lilin, topping)
       self.__rasa = rasa
       self.__warna_frosting = warna_frosting
       self.__harga = harga
    
    def get_rasa(self):
        return self.__rasa
    def get_warna_frosting(self):
        return self.__warna_frosting
    def get_harga(self):
        return self.__harga
    def __str__(self):
        return (f"""Kue sponge {self.__rasa} dengan topping {self.__topping} dan {self.__warna_frosting}
        Tulisan ucapan yang anda inginkan adalah {self.__tulisan}
        Angka lilin yang anda pilih adalah {self.__angka_lilin}
        Harga:  ¥{self.__harga}""")
    
class KueKeju(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        super().__init__(tulisan, angka_lilin, topping)
        self.__jenis_kue_keju = jenis_kue_keju
        self.__harga = harga

    def get_jenis_kue_keju(self):
        return self.__jenis_kue_keju
    def get_harga(self):
        return self.__harga
    def __str__(self):
        return (f"""Kue keju {self.__jenis_kue_keju} dengan topping {self.__topping}
        Tulisan ucapan yang anda inginkan adalah {self.__tulisan}
        Angka lilin yang anda pilih adalah {self.__angka_lilin}
        Harga:  ¥{self.__harga}""")

   

class KueBuah(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        super.__init__(tulisan, angka_lilin, topping)
        self.__jenis_kue_buah = jenis_kue_buah
        self.__harga = harga

    def get_jenis_kue_buah(self):
        return self.__jenis_kue_buah
    def get_harga(self):
        return self.__harga
    def __str__(self):
        return (f"""Kue buah {self.__jenis_kue_buah} dengan topping {self.__topping}
        Tulisan ucapan yang anda inginkan adalah {self.__tulisan}
        Angka lilin yang anda pilih adalah {self.__angka_lilin}
        Harga:  ¥{self.__harga}""")


# Fungsi ini harus diimplementasikan!
def buat_custom_bundle():
    run = True
    while run:
        print("""Jenis kue:
    1. Kue sponge
    2. kue keju
    3. kue buah
        """)

        tipe_kue = input("Pilih tipe kue")
        if tipe_kue.isdigit():
            tipe_kue = int(tipe_kue)
            # def __init__(self, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
            if tipe_kue == 1:
                warna_frosting = input("Pilih jenis warna frosting kue (Coklat/ Pink")
                rasa = input("pilih rasa kue (Coklat/ Stroberi)")
                kue_sponge = KueSponge(rasa,warna_frosting)
                return kue_sponge

            elif tipe_kue == 2:
                jenis = input("pilih jenis kue keju (New York/ Japanese")
                kue_keju = KueKeju(jenis)
                return kue_keju

            elif tipe_kue == 3:
                jenis = input("Pilih jenis kue buah (American / British")
                topping = input("Pilih topping (Ceri/ Stroberi")
                angka = int(input("Masukkan angka lilin: "))
                tulisan = input("masukkan tulisan pada kue: ")
                kue_buah = KueBuah(tulisan, angka, topping, jenis)
                return kue_buah
            else:
                break
# Fungsi ini harus diimplementasikan!
def pilih_premade_bundle():
    print("Pilihan Paket Istimewa: ")
    print("""1. New York-style Cheesecake with Strawberry Topping
2. Chocolate Sponge Cake with Cherry Topping and Blue Icing
3. American Fruitcake with Apple-Grape-Melon-mix Topping
        """)
    paket = int(input("pilih paket: "))
    angka = int(input("masukkan angka lilin: "))
    tulisan = input("Masukkan tulisan pada kue: ") 
    if paket == 1:
        return ("1. New York-style Cheesecake with Strawberry Topping", KueKeju(tulisan, angka))
    elif paket == 2:
        return ("2. Chocolate Sponge Cake with Cherry Topping and Blue Icing", KueSponge(tulisan, angka))
    elif paket == 3:
        return ("3. American Fruitcake with Apple-Grape-Melon-mix Topping", KueBuah(tulisan, angka))
# Fungsi ini harus diimplementasikan!
def print_detail_kue(kue):
    print("Berikut adalah kue pesanan anda: ")
    print(kue.__str__())

# Fungsi main jangan diubah!
def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")
        
        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")
    
    print("\nTerima kasih sudah berbelanja di Homura!")

if __name__ == "__main__":
    main()