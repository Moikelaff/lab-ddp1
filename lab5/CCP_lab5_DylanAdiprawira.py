menu = """
1. Penjumlahan
2. Pengurangan
3. Transpose
4. Determinan
5. Keluar
"""

# initialize buat matrix
run = True
print("Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang dapat dilakukan:")


while run:
    # input matrix 1
    size1 = input("Ukuran matriks 1: ")
    size1 = size1.split()
    row_matrix1 = int(size1[2])
    col_matrix1 = int(size1[0])
    
    matrix1_temp = []
    for i in range(row_matrix1):
        matrix1_temp.append(input(f"baris {i+1} matriks 1: "))
    
        if len(matrix1_temp[i]) <= (col_matrix1 * 2 - 1):
            continue
        else:
            print("terjadi kesalahan input")
            break
    # TODO: LOOPING /// CM BISA BUAT MATRIX 2 ROW
    matrix1 = []
    x1 = "".join(matrix1[0])
    x1 = x1.split()
    x1 = list(map(int, "".join(x1)))

    y1 = "".join(matrix1[1])
    y1 = y1.split()
    y1 = list(map(int, "".join(y1)))
    matrix1.append(x1)
    matrix1.append(y1)
     
    # input matrix 2
    size2 = input("Ukuran matriks 2: ")
    size2 = size2.split()
    row_matrix2 = int(size1[2])
    col_matrix2 = int(size1[0])
 
    matrix2_temp = []
    for i in range(row_matrix2):
        matrix2_temp.append(input(f"baris {i+1} matriks 1: "))
   
        if len(matrix2_temp[i]) <= (col_matrix2 * 2 - 1):
            continue
        else:
            print("terjadi kesalahan input")
            break
    
    # TODO: LOOPING /// CM BISA BUAT MATRIX 2 ROW
    matrix2 = []
    x2 = "".join(matrix2[0])
    x2 = x2.split()
    x2 = list(map(int, "".join(x2)))

    y2 = "".join(matrix2[1])
    y2 = y2.split()
    y2 = list(map(int, "".join(y2)))
    matrix2.append(x2)
    matrix2.append(y2)

    # function operation
    def penjumlahan():
        result = [[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]]
        for i in range(len(matrix1)):
            for j in range(len(matrix1[0])):
                result[i][j] = matrix1[i][j] + matrix2[i][j]
        
        for k in result:
            return (" ".join(map(str,k)))

    def pengurangan():
        result = [[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]]
        for i in range(len(matrix1)):
            for j in range(len(matrix1[0])):
                result[i][j] = matrix1[i][j] - matrix2[i][j]
        
        for k in result:
            return (" ".join(map(str,k)))

    def transpose():      
        row = len(matrix1)
        col = len(matrix1[0])

        transposed = []
        for i in range(col):
            row = []
            for j in range(row):
                transposed.append(row)
        return transposed
        
     # TODO WAJIB 2x2
    def determinan():
        return matrix1[0][0]*matrix1[1][1] - matrix1[1][0]*matrix1[0][1]

    def perkalian():
        result = [[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]]

        for i in range(len(matrix1)):
            for j in range(len(matrix2[0])):
                for k in range(len(matrix2)):
                    result[i][j] += matrix1[i][k] * matrix2[k][j]
        for l in result:
            return (" ".join(map(str,l)))


    #outputting
    print(menu)
    mode = input("Silahkan pilih operasi")
    if mode.isdigit():
        mode = int(mode)
        
        if mode == 1:
            penjumlahan()
        elif mode == 2:
            pengurangan()
        elif mode == 3:
            transpose()
        elif mode == 4:
            determinan()
        elif mode == 5:
            perkalian()
        elif mode == 6:
            run = False
            break

        else:
            print("mode hanya dari 1 sampai 5")
            print()

    else:
        print("input mode harus angka")

print("sampai jumpa!")