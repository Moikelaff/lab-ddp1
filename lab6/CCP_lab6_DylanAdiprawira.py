# initialize title label
title = """
==================================
WELCOME TO KARTI-NIAN CHALLENGE
==================================
"""
sorting_title = """
==================================
DATA MATA LOMBA DAN HADIAH
==================================
"""
challenger_title = """
==================================
DATA PESERTA CHALLENGE
==================================
"""
game_title = """
==================================
CHALLENGE DIMULAI
==================================
"""
result_title = """
==================================
FINAL RESULT
==================================
"""

# Pendataan awal dgn membuat list of dicts
print(title)
jml_lomba = int(input("Masukkan jumlah acara yang akan dilombakan: "))
lomba_total = []
for i in range(jml_lomba):
    print(f"LOMBA {i+1}")
    lomba = {}
    lomba["Nama Lomba"] = input("Nama Lomba: ")
    lomba["Hadiah Juara 1"] = int(input("Hadiah juara 1: "))
    lomba["Hadiah Juara 2"] = int(input("Hadiah juara 2: "))
    lomba["Hadiah Juara 3"] = int(input("Hadiah juara 3: "))
    print()
    lomba_total.append(lomba)
# Method Sorting menggunakan key "Nama Lomba"
sorted_lomba = sorted(lomba_total, key=lambda i: i["Nama Lomba"])


# outputting data lomba
print(sorting_title)
nama_lomba = [i["Nama Lomba"] for i in sorted_lomba]
nama_lomba.sort()
for i in range(len(nama_lomba)):
    print(nama_lomba[i])
    print("[Juara 1]", str(sorted_lomba[i]["Hadiah Juara 1"]))
    print("[Juara 2]", str(sorted_lomba[i]["Hadiah Juara 2"]))
    print("[Juara 3]", str(sorted_lomba[i]["Hadiah Juara 3"]))
    print()

# Data Peserta
print(challenger_title)
jml_peserta = int(input("Masukkan jumlah peserta yang akan dilombakan: "))
list_peserta = []
for i in range(jml_peserta):
    nama_peserta = input(f"Nama Peserta {i+1}: ").lower()
    if nama_peserta.isalpha():
        list_peserta.append(nama_peserta)
    else:
        print("Nama Peserta tidak Valid")
        break



# bagian challenge
print(game_title)
game_list_peserta = []
for i in nama_lomba:
    peserta = {}
    peserta["Juara 1"] = input("juara 1: ").lower()
    peserta["Juara 2"] = input("juara 2: ").lower()
    peserta["Juara 3"] = input("juara 3: ").lower()
    if (peserta["Juara 1"] in list_peserta) and (peserta["Juara 2"] in list_peserta) and (peserta["Juara 3"] in list_peserta):
        if (peserta["Juara 1"].isalpha()) and (peserta["Juara 2"].isalpha()) and (peserta["Juara 3"].isalpha()):
            game_list_peserta.append(peserta)
        else:
            print("Nama Peserta tidak valid")
            break
    else:
        print("Nama Peserta tidak ada di list peserta")
        break


# Bagian final result
print(result_title)
place = []
for game in range(len(nama_lomba)):
    for nama in list_peserta:
        if nama == peserta["Juara 1"]:
            total_hadiah = (lomba_total[game]["Hadiah Juara 1"])
            place.append(1)
        elif nama == peserta["Juara 2"]:
            total_hadiah = (lomba_total[game]["Hadiah Juara 2"])
            place.append(2)
        elif nama == peserta["Juara 3"]:
            total_hadiah = (lomba_total[game]["Hadiah Juara 3"])
            place.append(3)
    place = (", ".join(map(str,place)))
    print(nama)
    print(f"Total Hadiah {total_hadiah}")
    print(f"Juara yang pernah diraih{place}")