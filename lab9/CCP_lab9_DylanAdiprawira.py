import datetime
# parent class toko
class Toko:
    def __init__(self, cash=0, kue=0, deterjen=0):
        self.__cash = cash
        self.__kue = kue
        self.__deterjen = deterjen
    
    # getters
    def getCash(self):
        self.__cash += Kasir.getCash()
        return self.__cash
    def getKue(self):
        return self.__kue
    def getDeterjen(self):
        return self.__deterjen


# parent class user
class Karyawan:
    def __init__(self, username, password, name, umur, role):
        self.__username = username
        self.__password = password
        self.__name = name
        self.__umur = umur
        self.__role = role
        self.__logTime = ""

    # method register
    def register(self):
        print("Format data: [username] [password] [nama] [umur] [role]")
        reg_data = input("Input data karyawan baru: ")
        reg_data = reg_data.split()
        self.__username = reg_data[0].lower()
        self.__password = reg_data[1]
        self.__name = reg_data[2]
        self.__umur = reg_data[3]
        self.__role = reg_data[4]
        print(f"Karyawan {self.getUsername()} berhasil ditambahkan")
        user_dict = {"username": self.__username,
                     "password": self.__password,
                     "nama": self.__name,
                     "umur": self.__umur,
                     "role": self.__role}
            
    # getters
    def getUsername(self):
        return self.__username
    def getPassword(self):
        return self.__password
    def getName(self):
        return self.__name
    def getUmur(self):
        return self.__umur
    def getRole(self):
        return self.__role
    def getLogTime(self):
        return self.__logTime

    # method login
    def login(self):
        login_username = input("Username: ")
        login_password = input("Password: ")
        if (login_username != self.getUsername()) and (login_password != self.getPassword()):
            print("username/password salah. Silahkan coba lagi")
            login_username = input("Username: ")
            login_password = input("Password: ")        
        else:
            print(f"selamat datang {login_username}\n")
            login_choice = int(input("Apa yang ingin anda lakukan? (Tulis angka saja)"))
            print("3. Terima Pembayaran")
            print("10. logout")
            logtime = datetime.datetime.now()
            self.__logTime += logtime

            # branch role kasir
            if self.getRole().lower() == "kasir":
                if login_choice == 3:
                    Kasir.getCash()
                elif login_choice == 10:
                    print("LOGOUT BERHASIL")
                    run = False
                else:
                    print("pilihan tidak valid")

            # branch role janitor
            elif self.getRole().lower() == "janitor":
                if login_choice == 4:
                    Janitor.cleanToko()
                elif login_choice == 5:
                    Janitor.beliDeterjen()
                elif login_choice == 10:
                    print("LOGOUT BERHASIL")
                    run = False
                else:
                    print("pilihan tidak valid")
            # branch role chef
            elif self.getRole().lower() == "chef":
                if login_choice == 6:
                    Chef.buatKue()
                elif login_choice == 7:
                    Chef.buangKue()
                elif login_choice == 10:
                    print("LOGOUT BERHASIL")
                    run = False
                else:
                    print("perintah tidak valid")
            else:
                print("pilihan tidak valid")
                login_choice = int(input("Apa yang ingin anda lakukan? (Tulis angka saja)"))

    # method status report
    def statusReport(self):
        print("\n===================================")
        print(f"Jumlah Karyawan: ")
        print(f"jumlah Cash: {Toko.getCash()}")
        print(f"Jumlah Kue: {Toko.getKue()}")
        print(f"Jumlah deterjen: {Toko.getDeterjen()}")
        print(f"Terakhir kali dibersihkan: {Janitor.getCleanTime()}")
        print("===================================\n")

    # method karyawan report 
    def karyawanReport(self):
        print("\n===================================")
        print(f"Username: {Karyawan.getUsername()}")
        print(f"Nama: {Karyawan.getName()}")
        print(f"Umur: {Karyawan.getUmur()} ")
        print(f"Login Terakhir: {Karyawan.getLogTime()}")
        print(f"Role: {Karyawan.getRole()}")
        print(f"Jumlah kue yang telah dibuat: {Toko.getKue()}")
        print(f"Jumlah kue yang telah dibuang: {Chef.getBuangKue()} ")
        print("===================================\n")


# child class kasir
class Kasir(Karyawan, Toko):
    def __init__(self, username, password, name, umur, role, cash):
        super().__init__(username, password, name, umur, role, cash)
        self.__cash = cash
        
    def getCash(self):
        self.__cash = int(input("Jumlah Pembayaran: "))
        if not self.__cash.isdigit():
            print("input pembayaran tidak valid")
        else:
            print("Berhasil menerima uang sebanyak")
            return self.__cash


# child class janitor
class Janitor(Karyawan,Toko):
    def __init__(self, username, password, name, umur, role, deterjen):
        super().__init__(username, password, name, umur, role, deterjen)
        self.__cleanTime = ""
    
    def cleanToko(self):
        if self.__deterjen == 0:
            print("diperlukan setidaknya 1 deterjen untuk melakukan pembersihan")
        else:
            self.__deterjen -= 1
            clean_time = (datetime.datetime.now())
            self.__cleantime += clean_time
            print("Toko Berhasil dibersihkan")

    def beliDeterjen(self):
        amt = input("Jumlah pembelian deterjen: ")
        self.__deterjen += amt
        print(f"Berhasil membeli {self.__deterjen} deterjen")

    def getCleanTime(self):
        return self.__cleanTime

# child class chef
class Chef(Karyawan, Toko):
    def __init__(self, username, password, name, umur, role, kue):
        super().__init__(username, password, name, umur, role, kue)
        self.__buangKue = ""
    
    def buatKue(self):
        make = input("Buat berapa kue? ")
        self.__kue += make
        print(f"berhasil membuat {make} kue")

    def buangKue(self):
        throw = input("Buang berapa kue? ")
        self.__buangKue += str(throw)
        if throw > self.__kue:
            print("Tidak bisa membuang lebih banyak kue dibandingkan dengan stok kue yang ada sekarang")
            throw = input("Buang berapa kue? ")
        else:
            print(f"berhasil membuang {self.__kue} kue")

    def getBuangKue(self):
        return int(self.__buangKue)
    
# parent class Toko
class Toko:
    def __init__(self, cash=0, kue=0, deterjen=0):
        self.__cash = cash
        self.__kue = kue
        self.__deterjen = deterjen
    
    # getters
    def getCash(self):
        self.__cash += Kasir.getCash()
        return self.__cash
    def getKue(self):
        return self.__kue
    def getDeterjen(self):
        return self.__deterjen


def main():
    print("Selamat datang di Sistem Manajemen Homura\n")
    run = True
    while run:
        print("Apa yang ingin anda lakukan? (Tulis angka saja)")
        print("1. Register Karyawan Baru")
        print("2. Login")
        print("8. Status Report")
        print("9. Karyawan Report")
        print("11. Exit\n")

        choice = int(input("Pilihan: "))
        
        if choice == 1:
            Karyawan.register()
        elif choice == 2:
            Karyawan.login()
        elif choice == 8:
            Karyawan.statusReport()
        elif choice == 9:
            Karyawan.karyawanReport()
        elif choice == 11:
            print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")
            run = False
            break
        else:
            print("pilihan tidak valid")
            choice = int(input("pilihan: "))
 

if __name__ == '__main__':
    main()