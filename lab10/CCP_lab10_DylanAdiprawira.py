from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename

# TODO: Lengkapi class Application dibawah ini
class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.initUI()
        self.create_buttons()
        self.create_editor()
        master.title("Pacil Editor")
        master.geometry("600x600")

    def initUI(self):
        ### UI Title dan Geometry Saya pindahkan ke constructor ###
        
        self.frame = Frame(self)
        self.frame.pack()
        self.vert_scroll = Scrollbar(self.frame, orient=VERTICAL)
        self.vert_scroll.pack(side=RIGHT)
        self.vert_scroll = Scrollbar(self.frame, orient=HORIZONTAL)
        self.vert_scroll.pack(side=BOTTOM)

    def create_buttons(self):
        self.button_open = Button(self.frame, text='Open File', command= self.load_file)
        self.button_save = Button(self.frame, text='Save File', command= self.save_file)
        self.button_quit = Button(self.frame, text='Exit Program', command= self.destroy)

        self.button_open.grid(row=0, column=0)
        self.button_save.grid(row=0, column=1)
        self.button_quit.grid(row=0, column=2)

    def create_editor(self):
        self.txt_area = Text(self, width=50, height=30)
        self.txt_area.pack()

    def load_file_event(self, event):
        self.load_file()

    def load_file(self):
        file_name = askopenfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        text_file = open(file_name, 'r', encoding="utf-8")
        result = text_file.read()
        self.txt_area.insert(END, result)
        text_file.close()

    def save_file_event(self, event):
        self.save_file()

    def save_file(self):
        file_name = asksaveasfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        file_name.config(mode="w")
        result = str(self.txt_area.get())
        file_name.write(result)
        file_name.close()
        

    def set_text(self, text=''):
        self.edit.delete('1.0', END)
        self.edit.insert('1.0', text)
        self.edit.mark_set(INSERT, '1.0')
        self.edit.focus()

    def get_text(self):
        return self.edit.get('1.0', END+'-1c')


if __name__ == "__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()